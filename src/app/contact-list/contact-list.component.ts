import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  faPlusCircle,
  faTrash,
  faEdit,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { Contact } from '../contact';
// import { ContactFormComponent } from '../contact-form/contact-form.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css'],
})
export class ContactListComponent implements OnInit {
  contactArr: Array<Contact>;
  contact: Contact;
  // fontawesome
  faPlusCircle = faPlusCircle;
  faTrash = faTrash;
  faEdit = faEdit;
  faUser = faUser;
  // pagination
  page: number = 1;
  pageSize: number = 5;
  collectionSize: number = 0;
  // modal/form
  modalShowForm = true;
  modalAction: string;
  successMessage: boolean = false;

  constructor(
    private dataService: DataService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.dataService.getContacts().subscribe((res) => {
      this.contactArr = res;
      this.collectionSize = this.contactArr.length;
    });
  }

  showing() {
    return this.pageSize < this.collectionSize
      ? this.pageSize
      : this.collectionSize;
  }

  open(modal, contact: Contact) {
    this.contact = contact;
    this.modalService.open(modal);
  }

  save(modal, form) {
    // post output to console.log
    console.log('save', this.contact, form.value);

    // add to table
    if (!this.contact) {
      this.contactArr.push(form.value);
      // increment pagination
      ++this.collectionSize;
    } else {
      // update table
      this.contactArr = this.contactArr.map((contact) => {
        if (contact._id === form.value._id) {
          // replace contact data with form data
          contact = form.value;
        }
        return contact;
      });
    }

    // success message
    this.successMessage = true;
    setTimeout(() => {
      this.successMessage = false;
    }, 5000);

    // close modal
    modal.dismiss();
  }

  delete(modal) {
    this.contactArr = this.contactArr.filter(
      (contact) => contact._id !== this.contact._id
    );
    // decrement pagination
    --this.collectionSize;

    // update showing
    this.showing();

    // success message
    this.successMessage = true;
    setTimeout(() => {
      this.successMessage = false;
    }, 5000);

    // close modal
    modal.dismiss();
  }
}
