import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from './contact';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private REST_API_SERVER = 'http://demo5838836.mockable.io/contact';

  constructor(private httpClient: HttpClient) {}

  public getContacts(): Observable<Array<Contact>> {
    return this.httpClient.get<Array<Contact>>(this.REST_API_SERVER);
  }
}
