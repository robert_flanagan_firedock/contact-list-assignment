export class Contact {
  constructor(
    public _id: string,
    public index: number,
    public firstName: string,
    public lastName: string,
    public company: string,
    public email: string,
    public phone: string,
    public address: string
  ) {}
}
